<?php 

include('db.php');

$query = "SELECT * FROM products";
$result = mysqli_query($conn,$query);

include('header.php')

?>
        <!-- Product component table -->
        <?php if (isset($_SESSION['message'])){ ?>
        <div class="row">
            <div class="col-sm-5 mx-auto">
                <div class="alert alert-<?php echo $_SESSION['message_type'];?> alert-dismissible fade show" role="alert">
                    <?php echo $_SESSION['message'] ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
        <?php session_unset(); } ?>
        <div class="mt-5"></div>
        <div class="row">
            <div class="col-sm-10 mx-auto">
            <a href="new.php" class="btn btn-success mx-auto" name="new" type="submit">Nuevo</a>
            <div class="mt-2"></div>
                <table class="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th style="width: 18rem" >Descripción</th>
                            <th>Imagen</th>
                            <th>Precio</th>
                            <th style="width: 1rem">Oferta</th>
                            <th>Precio de oferta</th>
                            <th style="width: 9.5rem">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while($row = mysqli_fetch_assoc($result)){ ?>
                        <tr>
                            <th> <?php echo $row['id']; ?> </th>
                            <th> <?php echo $row['title']; ?> </th>
                            <th> <?php echo $row['description']; ?> </th>
                            <th> <img src="assets/img/<?php echo $row['img'];?>" alt="" style="width: 10rem"> </th>
                            <th> <?php echo $row['price']; ?> </th>
                            <th> <?php echo $row['off'];?></th>
                            <th> <?php echo $row['off-price']; ?> </th>
                            
                            <th>
                                <a href="index.php#<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-eye"></i></a>
                                <a href="mod.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-pen"></i></a>
                                <a href="controller/remove.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-trash"></i></a>
                            </th>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

</body>
<!-- Scripts Bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
        crossorigin="anonymous"></script>
</html>