<?php include('db.php');
include('header.php');    

$id = $_GET['id'];

$query = "SELECT * FROM products WHERE `id`=$id";
$result = mysqli_query($conn,$query);

$row = mysqli_fetch_assoc($result);
?>

    <div class="container">
    <div class="mt-5"></div>
        <div class="col-sm-8 mx-auto">
            <form action="controller/edit.php?id=<?php echo $id ?>" method="POST" enctype="multipart/form-data">
                <div class="d-flex">
                    <div class="col-sm-6 p-3">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" name="title" placeholder="Titulo del Producto" value="<?php echo $row['title'];?>" require>
                            <label for="floatingInputGrid">Titulo del Producto</label>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="form-floating m-3">
                        <textarea cols="76" name="description" placeholder="Descripción del Producto"
                            class="form-control" require><?php echo $row['description'];?></textarea>
                        <label for="floatingInputGrid">Descripción del Producto</label>
                    </div>
                </div>
                <div class="col-sm-8 m-3">
                    <img src="assets/img/<?php echo $row['img']; ?>" alt="" style="width: 10rem">
                    <input type="file" class="form-control" name="img" multiple>
                </div>
                <div class="d-flex">
                    <div class="form-floating m-3">
                        <input type="float" class="form-control" name="price" placeholder="Precio" value="<?php echo $row['price'];?>" require>
                        <label for="floatingInputGrid">Precio</label>
                    </div>
                </div>
                <div class="d-flex m-3">
                    <h3 class="text-white">Oferta</h3>
                </div>
                <div class="d-flex m-3">
                    <?php if($row['off'] == 0){ ?>
                    <input type="checkbox" name="off" value="1"/>
                    <?php } else { ?>
                    <input type="checkbox" name="off" value="1" checked/>
                    <?php } ?>
                </div>
                <div class="d-flex">
                    <div class="form-floating m-3">
                        <input type="float" class="form-control" name="off-price" placeholder="Precio de Oferta" value="<?php echo $row['off-price'];?>" require>
                        <label for="floatingInputGrid">Precio de Oferta</label>
                    </div>
                </div>
                <div class="d-flex m-5">
                    <button class="btn btn-primary col-sm-3 mx-auto" name="enviar" type="submit">Enviar</button>
                    <a href="dashboard.php" class="btn btn-danger col-sm-3 mx-auto">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>