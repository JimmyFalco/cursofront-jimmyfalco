<?php include('db.php');
$query = "SELECT * FROM products";
$result = mysqli_query($conn,$query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tienda Jimmy</title>
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- Estilos de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body class="bg-dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary" id="home">
        <div class="container-fluid">
            <a class="navbar-brand" href="#home">Tienda Jimmy</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" href="#info">Info</a>
                <a class="nav-link" href="#catalog">Catalogo</a>
            </div>
            </div>
        </div>
    </nav>
    <!-- info de la tienda -->
    <section>
        <div id="info" class="container">
            <div class="row mt-5 pb-5">
                <div class="col-sm-6 ml-5 p-5">
                    <h4 class="text-light text-center">Tienda jimmy los mejores precios del mercado.</h4>
                    <div class="mt-5"></div>
                    <ul class="list-unstyled text-secondary">
                        <li class="text-center">¿Por que deberia comprar con nosotros?</li>
                        <li class="mt-5"></li>
                            <ul>
                            <li>Tenemos el mejor precio ademas de la mejor atencion con una calificacion media de 4 estrellas hecha por nuestros clientes.</li>
                            <li class="list-unstyled mt-5"></li>
                            <li>Contamos con un sistema de devoluciones que si tiene algun problema con el producto nos hacemos responsables del costo que tenga el envio de 
                        devolucion en el caso que se demuestre el error.</li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6 ml-5 p-5">
                    <img src="assets/img/logotienda.jpg" style="width: 25rem; height: 20rem;" class="img-fluid" alt="...">
                </div>
            </div>
        </div>
    </section>
    
    <!-- Catalogo de productos -->
    <section id="catalog">
        <div class="container">
            <div class="row text-center">
            <?php while($row = mysqli_fetch_assoc($result)){ ?>
                <div class="col-sm-4" id="<?php echo $row['id'];?>">
                    <div class="card mx-auto mt-5" style="width: 18rem; height:
                            25rem;">
                        <div class="card-body">
                            <img src="assets/img/<?php echo $row['img']; ?>" alt="" style="width: 10rem">
                            <h4 class="card-title"> <?php echo $row['title']; ?> </h4>
                            <h6 class="card-text"> <?php echo $row['description']; ?> </h6>
                            <?php if($row['off'] == '0'){ ?>
                            <p class="card-text text-secondary mt-4">$<?php echo $row['price']; ?></p>
                            <?php }else{ ?>
                                <p class="card-text text-secondary"><del>$<?php echo $row['price']; ?> </del> &nbsp; &nbsp; $<?php echo $row['off-price']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    
</body>
</html>